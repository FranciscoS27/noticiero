<?php

    namespace Modelos;

    class Admin extends Conexion{
        public $usuario;
        public $password;

        public function __construct(){
            parent::__construct();
        }

        public function llenadoPost(){
            $this->usuario = $_POST['usuario'];
            $this->password = $_POST['password'];
        }

        public static function login($user, $contra){
            $conexion = new Conexion;
            $pre = mysqli_prepare($conexion->con, "SELECT* FROM administradores WHERE usuario = ? AND contrasennia = ?");
            $pre->bind_param("ss", $user, $contra);
            $pre->execute();
            $resultado = $pre->get_result();
            return $resultado->fetch_object();
        }

        public function crear(){
            $this->llenadoPost();
            $pre = mysqli_prepare($this->con, "INSERT INTO administradores(usuario,contrasennia) VALUES (?,?)");
            $pre->bind_param("ss", $this->usuario, $this->password);
            $pre->execute();
        }

    }


?>