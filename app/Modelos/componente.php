<?php

    namespace Modelos;

    class Componente extends Conexion{
        public $componente;
        public $descripcion;
        public $imagen; 

        public function __construct(){
            parent::__construct();
        }

        public function llenadoPost(){
            $this->componente = $_POST['componente'];
            $this->descripcion = $_POST['descripcion'];
            if(isset($_FILES['imagen'])){
                $this->imagen = $_FILES['imagen']['name'];
            }
            elseif(isset($_POST['imagenNombre'])){
                $this->imagen = $_POST['imagenNombre'];
            }
        }

        public function crear(){
            $this->llenadoPost();
            $pre = mysqli_prepare($this->con, "INSERT INTO componentes(nombre, descripcion, imagen) VALUES (?, ?, ?)");
            $pre->bind_param("sss", $this->componente, $this->descripcion, $this->imagen);
            $pre->execute();
        }

        public static function consultar($componenteN){
            $conexion = new Conexion;
            $pre = mysqli_prepare($conexion->con, "SELECT* FROM componentes WHERE nombre = ?");
            $pre->bind_param("s", $componenteN);
            $pre->execute();
            $resultado = $pre->get_result();
            return $resultado->fetch_object();
        }

        public static function consultarTodo(){
            $conexion = new Conexion;
            $pre = mysqli_prepare($conexion->con, "SELECT* FROM componentes");
            $pre->execute();
            $resultado = $pre->get_result();
            return $resultado;
        }


        public function editar($componenteN){
            $this->llenadoPost();
            $pre = mysqli_prepare($this->con, "UPDATE componentes SET nombre = ?, descripcion= ?, imagen= ? WHERE nombre = ?");
            $pre->bind_param("ssss", $this->componente, $this->descripcion, $this->imagen, $componenteN);
            $pre->execute();
        }
        
        public function eliminar($componenteN){
            $pre = mysqli_prepare($this->con, "DELETE FROM componentes WHERE nombre = ?");
            $pre->bind_param("s", $componenteN);
            $pre->execute();
        }
        

    }

?>