<!DOCTYPE html>
<?php use Modelos\Componente; ?>
<html lang="en">
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WikiPC</title>
</head>
<body>
    <div>
        <?php
            if(isset($_SESSION['admin'])){
                echo "<div style='padding:1% 10%; max-height:80px;'>
                    <a class='btn btn-primary' style='margin-left:auto;margin-right:auto;display:block; margin-bottom:5px' href='../noticiero/index.php?controller=user&action=vistaAdmin'>Vista administrador</a><br>
                </div>";
            }
        ?>
    </div>
    <div>
        <!--Navegador-->
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="margin-bottom: 20px; padding:0 2%">
            <div class="container-fluid">
                <a class="navbar-brand" href="../noticiero/index.php?controller=user&action=vistaLogin"><h2>WikiPC</h2></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">       
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-bs-toggle="collapse" data-bs-target="#compInt" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="true">
                              Componentes internos
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown" id="compInt">
                              <li><a class="dropdown-item" href="../noticiero/index.php?controller=user&action=info&componente=GPU">GPU</a></li>
                              <li><a class="dropdown-item" href="../noticiero/index.php?controller=user&action=info&componente=CPU">CPU</a></li>
                              <li><a class="dropdown-item" href="../noticiero/index.php?controller=user&action=info&componente=Fuente">Fuente</a></li>
                              <li><a class="dropdown-item" href="../noticiero/index.php?controller=user&action=info&componente=RAM">RAM</a></li>
                              <li><a class="dropdown-item" href="../noticiero/index.php?controller=user&action=info&componente=Targeta-madre">Targeta Madre</a></li>
                              
                              
                              <li><a class="dropdown-item" href="../noticiero/index.php?controller=user&action=info&componente=HDD">HDD</a></li>
                              <li><a class="dropdown-item" href="../noticiero/index.php?controller=user&action=info&componente=SDD">SSD</a></li>
                              
                              
                              <li><a class="dropdown-item" href="../noticiero/index.php?controller=user&action=info&componente=Disipador-de-aire">Disipador Aire</a></li>
                              <li><a class="dropdown-item" href="../noticiero/index.php?controller=user&action=info&componente=Disipador-liquido">Disipador liquido</a></li>
                              <li><a class="dropdown-item" href="../noticiero/index.php?controller=user&action=info&componente=Disipador-de-aceite">Disipador aceite </a></li>
                              
                              <li><hr class="dropdown-divider"></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-bs-toggle="collapse" data-bs-target="#compExt" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Componentes externos
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown" id="compExt">
                              <li><a class="dropdown-item" href="../noticiero/index.php?controller=user&action=info&componente=Teclado">Teclado</a></li>
                              <li><a class="dropdown-item" href="../noticiero/index.php?controller=user&action=info&componente=Monitor">Monitor</a></li>
                              <li><a class="dropdown-item" href="../noticiero/index.php?controller=user&action=info&componente=Raton">Raton</a></li>
                              <li><a class="dropdown-item" href="../noticiero/index.php?controller=user&action=info&componente=Gabinete">Gabinete</a></li>
                              <li><hr class="dropdown-divider"></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" 
                               data-bs-toggle="collapse" 
                               data-bs-target="#soft" 
                               id="navbarDropdown" 
                               role="button" 
                               data-bs-toggle="dropdown" 
                               aria-expanded="false">
                                Software
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown" id="soft">
                              <li><a class="dropdown-item" href="../noticiero/index.php?controller=user&action=info&componente=NOS">SO de red</a></li>
                              <li><a class="dropdown-item" href="../noticiero/index.php?controller=user&action=info&componente=BIOS">BIOS</a></li> 
                              <li><a class="dropdown-item" href="../noticiero/index.php?controller=user&action=info&componente=Teclado">SO</a></li>

                              <li><hr class="dropdown-divider"></li> 
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!--Zona de informacion obtenida de la BD-->
        <div>
          <?php
            if(isset($_GET['componente'])){
              $componente = Componente::consultar($_GET['componente']); 
              if($componente != ""){
                $imagen = $componente->imagen; 
                echo "<img style='max-height: 300px; max-width: 80%; margin-left: auto; margin-right:auto; display:block;' src='../noticiero/public/imagenes/$imagen'>";
                echo "<h1 style='text-align:center'>
                  $componente->descripcion
                </h1>";
              }
              else{
                echo "<img style='max-height: 300px; max-width: 80%; margin-left: auto; margin-right:auto; display:block;' src='../noticiero/public/imagenes/logo.png'>";
              }
            }
            else{
              echo "<img style='max-height: 300px; max-width: 80%; margin-left: auto; margin-right:auto; display:block;' src='../noticiero/public/imagenes/logo.png'>";
            }
          ?>
        </div>
    </div>
</body>
</html>