<!DOCTYPE html>
<html lang="en">
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>administrador</title>
</head>
<body>
    <div style="padding:5% 10%">
        <a class="btn btn-primary" style="margin-left:auto;margin-right:auto;display:block; margin-bottom:10px" href="../noticiero/index.php?controller=user&action=vistaLogin">Vista login</a><br>
        <a class="btn btn-primary" style="margin-left:auto;margin-right:auto;display:block; margin-bottom:10px" href="../noticiero/index.php?controller=user&action=vistaEditar">Vista editar componente</a><br>
        <a class="btn btn-primary" style="margin-left:auto;margin-right:auto;display:block; margin-bottom:10px" href="../noticiero/index.php?controller=user&action=vistaRegistrar">Vista registrar componente</a><br>
        <a class="btn btn-primary" style="margin-left:auto;margin-right:auto;display:block; margin-bottom:10px" href="../noticiero/index.php?controller=user&action=info">Vista de usuario</a><br>
        <a class="btn btn-primary" style="margin-left:auto;margin-right:auto;display:block; margin-bottom:10px" href="../noticiero/index.php?controller=user&action=vistaRegistrarAdmin">Vista de registro de administrador</a><br>
        <a class="btn btn-primary" style="margin-left:auto;margin-right:auto;display:block; margin-bottom:10px" href="../noticiero/index.php?controller=user&action=logOut">Logout</a><br>
        <a href=""></a>
    </div>
    
</body>
</html>