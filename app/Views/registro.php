<!DOCTYPE html>
<html lang="en">
    <head>
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
      <link rel="stylesheet" href="../noticiero/public/css/registro.css">
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>registro</title>
    </head>
    <body>
        <div>
            <?php
                if(isset($_SESSION['admin'])){
                    echo "<div style='padding:5% 10%; max-height:50px;'>
                        <a class='btn btn-primary' style='margin-left:auto;margin-right:auto;display:block; margin-bottom:5px' href='../noticiero/index.php?controller=user&action=vistaAdmin'>Vista administrador</a><br>
                    </div>";
                }
            ?>
        </div>
        <form method="POST" action="../noticiero/index.php?controller=user&action=registrarAdmin" style="padding:5% 10%">
        <section>
            <div class="form-group">
                <label>Nombre</label>
                <input type="text" class="form-control" name="usuario" placeholder="Ej. Juan">
                <small id="hNombre" class="form-text text-muted">Introduse tu nombre.</small>
            </div>
            <div class="form-group" style="margin-bottom:15px">
                <label for="exampleInputPassword1">Contraseña</label>
                <input type="password" class="form-control" id="contrasena" name='password' placeholder="contraseña123">
            </div>
            <button type="submit" class="btn btn-primary" style="margin-left:auto; margin-right:auto; display: block;">Enviar</button>
        </section>
        </form>
        <footer style="position: fixed; bottom: 0; width:100%">&copy; 2021, Todos los derechos reservados.</footer>
    </body>
 </html>