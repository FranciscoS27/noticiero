<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../noticiero/public/css/login-css.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <title>Iniciar sesion</title>
</head>
<body>
    <?php
        if(isset($_SESSION['admin'])){
            header("location:../noticiero/index.php?controller=user&action=vistaAdmin");
        }
    ?>
    <div class="wrapper fadeInDown" style="padding:5% 15%">
        <div id="formContent">
            <!-- Tabs Titles -->
            <?php
                if(isset($_GET['failure'])){
                    echo "<center><h1>Datos incorrectos</h1></center>";
                }
            ?>
            <!-- Icon -->
            <div class="fadeIn first" style="margin: 15px 0px">
              <img style="margin-left:auto; margin-right:auto; display: block; " src="../noticiero/public/imagenes/icono-usuario.png" width="auto" height="80px" alt="User Icon">
            </div>
        
            <!-- Login Form -->
            <form method="POST" action="../noticiero/index.php?controller=user&action=login">
              <input type="text" class="form-control" name="usuario" placeholder="Usuario"><br>
              <input type="password" class="form-control" name="password" placeholder="Contraseña"><br>
              <input type="submit" class="btn btn-primary" style="margin-left:auto; margin-right:auto; display: block;" value="Log In">
            </form>      
        </div>
    </div>
</body>
</html>
