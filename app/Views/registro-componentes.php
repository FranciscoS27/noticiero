<!DOCTYPE html>
<html lang="en">
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<link rel="stylesheet" href="../noticiero/public/css/registro-componentes.css">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro de componentes</title>
</head>
    <body>
        <div>
            <?php
                if(isset($_SESSION['admin'])){
                    echo "<div style='padding:5% 10%; max-height:50px;'>
                        <a class='btn btn-primary' style='margin-left:auto;margin-right:auto;display:block; margin-bottom:5px' href='../noticiero/index.php?controller=user&action=vistaAdmin'>Vista administrador</a><br>
                    </div>";
                }
            ?>
        </div>
        <form style="padding:5% 15%;" enctype="multipart/form-data" method="POST" action="../noticiero/index.php?controller=user&action=registrarComponente">
                <div class="form-grup">
                    <label >Nombre</label>
                    <input type="text" class="form-control" name="componente" placeholder="Nombre del componente">
                    <small id="hNombre" class="form-text text-muted">verifica que sea correcto.</small>
                </div>
                <div class="form-grup">
                    <label >Subir imagen del componente</label>
                    <br>
                    <input type="file" name="imagen" style="max-width: 100%">
                    <br>
                    <small id="hImagen" class="form-text text-muted">verifica que sea correcto.</small>
                </div>
                
                <div class="form-group">
                    <label>Descripcion</label>
                    <textarea class="form-control" name="descripcion" rows="3" style="resize:none; margin-bottom:10px"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Registrar</button>
        </form>
        <footer style="position: fixed; bottom: 0; width:100%">&copy; 2021, Todos los derechos reservados.</footer>
    </body>
</html>