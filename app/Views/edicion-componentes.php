<!DOCTYPE html><!DOCTYPE html>
<html lang="en">
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<link rel="stylesheet" href="../noticiero/public/css/edicion-componentes.css">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edicion de componentes</title>
    
</head>
    <body>
        <?php use Modelos\Componente; ?>
        <div>
        <?php
            if(isset($_SESSION['admin'])){
                echo "<div style='padding:5% 10%; max-height:50px;'>
                    <a class='btn btn-primary' style='margin-left:auto;margin-right:auto;display:block; margin-bottom:5px' href='../noticiero/index.php?controller=user&action=vistaAdmin'>Vista administrador</a><br>
                </div>";
            }
        ?>
        </div>
        <?php 
            if(isset($_POST['componente'])){
                if($_POST['componente'] != "0"){
                    $componente = Componente::consultar($_POST['componente']);
                    $original = $_POST['componente'];
                    echo "<section>
                        <form style='padding:5% 15%;' enctype='multipart/form-data' method='POST' action='../noticiero/index.php?controller=user&action=editarComponente'>
                            <input type='hidden' name='nombreComponente' value='$componente->nombre'>
                            <div class='form-grup'>
                                <label >Nombre</label>
                                <input type='text' class='form-control' name='componente' Value='$componente->nombre'>
                            </div>
                            <div class='form-grup'>
                                <label >Imagen del componente</label><br>
                                <img src='../noticiero/public/imagenes/$componente->imagen' style='width:300px'><br>
                                <input type='hidden' name='imagenNombre' value='$componente->imagen'>
                                <br>
                            </div>
                            <div class='form-group'>
                                <label>Descripcion</label>
                                <textarea class='form-control' name='descripcion' rows='3' style='resize:none; margin-bottom:10px'>$componente->descripcion</textarea>
                            </div>
                            <button type='submit' class='btn btn-primary'>Actualizar</button>
                        </form>
                        <br>
                        <form style='padding:5% 15%;' enctype='multipart/form-data' method='POST' action='../noticiero/index.php?controller=user&action=eliminarComponente'>
                            <input type='hidden' name='componente' value='$componente->nombre'>
                            <button type='submit' class='btn btn-primary'>Eliminar</button>
                        </form>
                    </section>";
                }
                else{
                    header("location:../noticiero/index.php?controller=user&action=vistaEditar&failure");
                }
            }
            else{
                if(isset($_GET['failure'])){
                    echo "<center><h1>Componente no seleccionado</h1></center>";
                }
                elseif(isset($_GET['success'])){
                    echo "<center><h1>Componente actualizado</h1></center>";
                }
                echo"<form style='padding:5% 20%' method='POST' action='../noticiero/index.php?controller=user&action=vistaEditar'>
                    <select style='margin-bottom:10px; width:100%' name='componente'>
                        <option value='0'>Seleccione componente</option>";                        
                            $componentes = Componente::consultarTodo();
                            while ($valores = mysqli_fetch_array($componentes)) {
                                echo "<option value=$valores[nombre]>$valores[nombre]</option>";
                            }
                    echo"</select><br>
                    <button type='submit' class='btn btn-primary' style='margin-left:auto;margin-right:auto;display:block;'>Buscar</button>
                </form>";
            }
        ?>
        <br>


        <footer style="position: fixed; bottom: 0; width:100%">&copy; 2021, Todos los derechos reservados.</footer>
    </body>
</html>