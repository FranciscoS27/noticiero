<?php
    include "app/Modelos/conexion.php";
    include "app/Modelos/componente.php";
    include "app/Modelos/administrador.php";
    use Modelos\Conexion;
    use Modelos\Componente;
    use Modelos\Admin;

    class userController{

        //Constructor para evitar la entrada a las vistas de control para administrador
        public function __construct(){
            session_start();
            if($_GET['action'] == 'vistaRegistrar' || $_GET['action'] == 'vistaEditar' || $_GET['action'] == 'registrarComponente' || $_GET['action'] == 'eliminarComponente' || $_GET['action'] == 'editarComponente'){
                if(isset($_SESSION["admin"])){
                }
                else{
                    header("location:../noticiero/index.php?action=vistalogin&controller=user&nosession");
                }
            }
        }

        //Vista publica de la pagina, unicamente informativa
        public function info(){
            require "app/Views/principal.php";
        }

        public function vistaRegistrarAdmin(){
            require "app/Views/registro.php";
        }

        public function registrarAdmin(){
            $admin = new Admin;
            $admin->crear();
            require "app/Views/login.php";
        }

        //Vista para registrar en base de datos un nuevo componente
        public function vistaRegistrar(){
            require "app/Views/registro-componentes.php";
        }

        //Funcion para registrar en base de datos un nuevo componente
        public function registrarComponente(){
            if(isset($_POST['descripcion']) && isset($_FILES['imagen']) && isset($_POST['componente'])){   
                if($_POST['descripcion'] != "" && $_FILES['imagen'] != "" && $_POST['componente'] != ""){
                    $direccion = "public/imagenes/".$_FILES['imagen']['name'];
                    if(move_uploaded_file($_FILES['imagen']['tmp_name'], $direccion)) {
                        $componente = new Componente;
                        $componente->crear();
                        header("location:../noticiero/index.php?action=vistaRegistrar&controller=user&success");
                    } else{
                        echo "no lo sé master";
                        header("location:../noticiero/index.php?action=vistaRegistrar&controller=user&failure");
                    }
                }
                else{
                    header("location:../noticiero/index.php?action=vistaRegistrar&controller=user&failure");
                }
            }
            else{
                header("location:../noticiero/index.php?action=vistaRegistrar&controller=user&failure");
            }
        }

        //Vista para editar en base de datos un componente ya creado
        public function vistaEditar(){
            require "app/Views/edicion-componentes.php";
        }

        //Funcion para editar en base de datos un componente ya creado
        public function editarComponente(){
            if(isset($_POST['descripcion']) && (isset($_FILES['imagen']) || isset($_POST['imagenNombre'])) && isset($_POST['componente']) && isset($_POST['nombreComponente'])){   
                if($_POST['descripcion'] != "" && ($_FILES['imagen'] != "" || $_POST['imagenNombre'] != "") && $_POST['componente'] != "" && $_POST['nombreComponente'] != ""){
                    if($_FILES['imagen'] != ""){
                        $direccion = "public/imagenes/".$_FILES['imagen']['name'];
                        if(move_uploaded_file($_FILES['imagen']['tmp_name'], $direccion)){
                            $componente = new Componente;
                            $componente->editar($_POST['nombreComponente']);
                            header("location:../noticiero/index.php?action=vistaEditar&controller=user&success");
                        }
                        else{
                            header("location:../noticiero/index.php?action=vistaEditar&controller=user&failure");
                        }
                    }
                    elseif($_POST['imagenNombre'] != ""){
                        $componente = new Componente;
                        $componente->editar($_POST['nombreComponente']);
                        header("location:../noticiero/index.php?action=vistaEditar&controller=user&success");
                    }
                }
                else{
                    header("location:../noticiero/index.php?action=vistaEditar&controller=user&failure");
                }
            }
            else{
                header("location:../noticiero/index.php?action=vistaEditar&controller=user&failure");
            }
        }

        //Funcion para eliminar en base de datos un componente ya creado
        public function eliminarComponente(){
            if(isset($_POST['componente'])){
                $componente = new Componente;
                $componente->eliminar($_POST['componente']);
                header("location:../noticiero/index.php?action=vistaEditar&controller=user&success");
            }
        }

        //Vista para inicio de sesion de un administrador
        public function vistaLogin(){
            require "app/Views/login.php";
        }

        //Funcion para comparar datos de sesion con un administrador registrado
        public function login(){
            session_start();
            if(isset($_POST['password']) && isset($_POST['usuario'])){
                $contrasennia = $_POST['password'];
                $usuario = $_POST['usuario'];

                $logeo = Admin::login($usuario, $contrasennia);
                $_SESSION["admin"] = $logeo;
                
                if(isset($_SESSION["admin"])){
                    header("location:../noticiero/index.php?controller=user&action=vistaAdmin");
                }
                else{
                    header("location:../noticiero/index.php?action=vistalogin&controller=user&failure");
                }
            }
            else{
                header("location:../noticiero/index.php?action=vistalogin&controller=user&failure");
            }
        }

        //Funcion para cerrar sesion de administrador
        public function logOut(){
            if(isset($_SESSION['admin'])){
                unset($_SESSION['admin']);
            }
            require "app/Views/login.php";
        }

        //Vista de un menu hacia vistas unicas de administrador
        public function vistaAdmin(){
            require "app/Views/edicion.php";
        }

    }

?>